(ns leiningen.major-project-version
  (:require [clojure.string :refer [split]]))


(defn ^:no-project-needed major-project-version
  "Prints project major version"
  [project & keys]
  (leiningen.core.main/info (first (split (:version project) #"\.")))
  (flush))
